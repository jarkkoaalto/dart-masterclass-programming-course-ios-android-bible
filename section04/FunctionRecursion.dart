void main(){
  // 5! = 5*4*3*2*1 = 120
  // 6! = 6*5*4*3*2*1 = 720
  int res = CalFactorial(12);
  print(res);
}

int CalFactorial(int n){

  if(n <= 0){
    return 1;
  }else{
    int result = (n*CalFactorial(n-1));
    return result;
  }
}