void main(){
  Set testSet = new Set();

  testSet.add(5);
  testSet.add(15);
  testSet.add(25);
  testSet.add(35);

  print(testSet);
  print(testSet.last);

  for(var value in testSet){
    print(value);
  }

  Set testSet2 = new Set.from([2,3,4,5,6]);
  print(testSet2);
}