
import 'dart:collection';

void main(){
  // FiFo (First in, first out)
  Queue q = new Queue();
  q.add(3443);
  q.add(8764);
  q.add(1234);
  q.add(6743);
  q.add(8821);
  q.add(3235);
  q.add(6564);

  for(var i in q){
    print(i);
  }

  q.addFirst(9999);
  q.addLast(1000);

  for(var s in q){
    print(s);
  }
}