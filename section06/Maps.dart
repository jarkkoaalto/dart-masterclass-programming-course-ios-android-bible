void main()
{
  var testMap = {'key1':1024,'key2':5605,'key3':6077};
    print(testMap);
    print(testMap['key2']);

    testMap.forEach((key, value) => print("$key and $value"));

    print("\n*********************\n");

    var koeMap = new Map();
    koeMap['key1'] = 2455;
    koeMap['key2'] = 6764;
    koeMap['key3'] = 8877;
    koeMap['key4'] = 4643;
    koeMap['key5'] = 1002;

    koeMap.forEach((key, value) => print("$key and $value"));
}