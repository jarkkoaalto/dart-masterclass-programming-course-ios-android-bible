void main()
{
  var scores = new List(5);

  scores[0] = 10;
  scores[1] = 100;
  scores[2] = 1000;
  scores[3] = 10000;
  scores[4] = 100000;

  for(int i=0; i<scores.length;i++){
    print(scores[i]);
  }
// OR
print("\n******\n");
 var numbers = new List();
  numbers.add(10);
  numbers.add(100);
  numbers.add(1000);
  numbers.add(10000);
  numbers.add(100000);

  for(int i=0; i<numbers.length;i++){
    print(numbers[i]);
  }
}