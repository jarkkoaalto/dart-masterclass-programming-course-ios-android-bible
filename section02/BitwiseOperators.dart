void main(){
  int num1 = 55;
  int num2 = 127;

  // Bitwise AND &
  print(num1 & num2); // 55

  // Bitwise OR |
  print(num1 | num2); // 127

  // Bitwise XOR ^
  print(num1 ^ num2);

  //BItwise not ~

  // left sift <<
  print(num1 << 3);

  // right sift >>
  print(num1 >> 3);

}