void main(){
  int i = 10;
  // Is type
  print(i is String);

  // Is not type
  print(i is! String);

}