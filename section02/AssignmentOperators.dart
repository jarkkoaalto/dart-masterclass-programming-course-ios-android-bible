void main(){

  // Assignnment
  int i = 20;

  // NULL only assignment ??=
  // int j = null;
  int j = 6;
  j ??= 10;
  print(j);

  // Add assignment +=
  int num1 = 10;
  print(num1);
  num1 += 5; // num1 = num1 + 5;
  print(num1);

  num1 -= 5; // num1 = num1 - 5;
  print(num1);

  num1 *= 5; // num1 = num1 * 5;
  print(num1);
}