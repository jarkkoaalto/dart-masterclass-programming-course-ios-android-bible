class Epic
{
  var status = 0;
  static var staticS = 0;


epicFun() {
  status++;
  staticS++;

  print('status $status & staticS $staticS');
  }
}


void main(){
    Epic e = new Epic();
    e.epicFun();
    e.epicFun();
    e.epicFun();
    e.epicFun();
    print("\n\n");
    Epic e2 = new Epic();
    e2.epicFun();
    e2.epicFun();
    e2.epicFun();
    e2.epicFun();

}