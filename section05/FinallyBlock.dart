void main(){

  int num1 = 10;
  int num2 = 0;

  String pi = "3.1415";

  try{
    print(num1 ~/ num2);
    double number = double.parse(pi);
    print(number * number);
  }
  catch(error) {
    print("Catch block ${error}");
  }
  finally{
    print("Finally");
  }
  print("End of Application");

}