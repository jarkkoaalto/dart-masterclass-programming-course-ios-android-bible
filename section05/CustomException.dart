
class PracticeException implements Exception
{
  String errMsg() => "Practice Exception";
}
void main(){
  int num1 = 100;
  int num2 = 5;

  try{
    if(num1 == 100){
      throw new PracticeException();
    }else{
      print(num1 ~/ num2);
    }
  }
  on FormatException {
    print("Number cannot be 100");
  }
  catch(error){
    print("Catch block ${error}");
  }
  finally{
    print("Finally block");
  }
  print("End of Application");
}